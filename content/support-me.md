---
title: 'Support Me'
---
Have any of my blog posts saved you time, and taught you some great time-saving tricks? Have they helped you close off that piece of work a little bit quicker, or made your tooling more secure?

I maintain a number of [Open Source projects](/open-source/) that you may be using, or IndieWeb-related contributions or services you depend on?

I'd appreciate if you paid it forwards by either contributing your own blogs to the world, or [supporting me on Buy Me a Coffee](https://www.buymeacoffee.com/jamietanna) - you can also support me through [PayPal.me](https://paypal.me/jamiet) or [Monzo.me](https://monzo.me/jamesvivektanna/5).

I'm a huge fan of Gousto, and have [written about how much I enjoy it](/posts/2019/07/07/four-months-gousto/), and you may too - you can sign up with my [referral code `JAMIE21201650`](https://cook.gousto.co.uk/raf?promo_code=JAMIE21201650&utm_source=androidapp), which helps keep me health(ier) and benefits both of us!

Also:

- Buy [a Stimagz fidget toy with my referral code](https://www.stimagz.com/?bon_ref=2dW5lwrd)
