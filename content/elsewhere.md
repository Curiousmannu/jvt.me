---
title: 'Elsewhere'
---
You can find me on other platforms:

- <a rel="me" href="https://gitlab.com/jamietanna"><i class="fa fa-gitlab" title="GitLab.com Profile"></i>&nbsp;@jamietanna on GitLab</a>
- <a rel="me" href="https://github.com/jamietanna"><i class="fa fa-github" title="GitHub.com Profile"></i>&nbsp;@jamietanna on GitHub</a>
- <code rel=me>@www.jvt.me@www.jvt.me</code> on the Fediverse/Mastodon, which mirrors the feed that's visible [from my home page](/)
- <a rel="me" href="https://twitter.com/jamietanna"><i class="fa fa-twitter" title="Twitter.com Profile"></i>&nbsp;@jamietanna</a> ([although now unused](https://www.jvt.me/mf2/2023/04/ein9i/))
- <a rel="me" href="https://linkedin.com/in/jamietanna"><i class="fa fa-linkedin" title="LinkedIn Profile"></i>&nbsp;Jamie Tanna</a>
- <a rel=me href="mailto:hi@jamietanna.co.uk"><i class="fa fa-envelope" title="Email Address"></i>&nbsp;hi@jamietanna.co.uk on email</a>
